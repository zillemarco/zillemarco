# A little bit about me

I was born on April 15th, 1994 in Pordenone (Italy) where I currently live.

Since when I was young, I always had three passions: cooking, computers and playing guitar.

I still do a lot of cooking (funny story, I love cooking so much that I almost went to cooking school
instead of IT :sweat_smile:), and occasionally play guitar (not too much free time unfortunately),
but my main thing is, of course, coding! :smile:

And so I went to IT school where as a work-school experience I started working at MEDIASTUDIO where,
where after I finished school, I got employed as a fulltime Software Engineer.

While working, I also went to the University of Udine, where I got my Bachelor Degree in Information Technology.

My current role in MEDIASTUDIO is a mix of various different roles:

- I mostly do R&D on new products and technologies
- I developed and maintain a 3D engine used by our desktop products
- I am the maintainer for all the DevOps we do
- some other stuff that I don't remember :sweat_smile:

Recently, 24th September 2022 (so I don't forget :rofl:), I also got married with my amazing wife Jessica :tada: :smile:

## IT knowledge

I am mostly a desktop Software Engineer for the Windows ecosystem, but in later years I also worked on
some web applications so I also am kindof a FullStack Engineer. Oh, and I also have some background
developing mobile applications, mostly using Xamarin Forms :slight_smile:.

These are the frameworks I am mostly familiar with:

- MFC
- Win32
- .NET
- React
- MS-SQL
- Xamarin Forms
- Ruby (thanks GitLab :sweat_smile:)
- Vue (again, thanks GitLab :sweat_smile:)

These are the languages that I know:

- Strong knowledge:
  - English (:stuck_out_tongue_closed_eyes:)
  - Italian (:rofl:)

Seriously now :smile:

- Strong knowledge:
  - C
  - C++
  - C#
- Decent knowledge:
  - JavaScript
  - TypeScript
  - Python
  - Ruby
  - TSQL
- Some knowledge:
  - PHP
  - Objective C
  - Swift
  - Java

# My GitLab journey

I started using GitLab a few years back, but not much. Then, about two years ago I decided to do a little experiment
in our office.

I started hearing more and more about Git and its "power" in combination with CI/CD and all DevSecOps stuff,
and I was kind of familiar with it for personal projects via GitHub. I then talked with some colleagues and decide to try
and migrate some of our projects over to Git, we were using SVN at that time, but we wanted to have our own instance, so
the solution was to install a Self-Hosted instance of GitLab.

Move on one year and we migrated basically all our projects from SVN over to Git, and GitLab, and started using
all those cool CI/CD stuff that intrigued me so much.

Move on a little bit more, to March 2022, where I finally have a presentation with the rest of my colleagues
showcasing GitLab and how to start using it at its full potential: they where already using it, but only as a Git backend,
since we kept using our other tools for issues tracking, time tracking, etc.

During this showcase, they raised some concerns about some missing or incomplete features that prevented us from
completely switching over to GitLab as a single sort of truth for everying (work planning, issue tracking, time tracking, etc.),
so I opened a couple of issues on GitLab to request those missing features.

A few days later, Lee Tickett reached out to me, we discussed about those requests and he kindly offered to
introduce me to contributing to GitLab, showing me how it works, how to open MRs, etc. and I really needed
that initial help because, well, I never used Ruby or Vue before contributing to GitLab :sweat_smile:
(PS: this is a perfect example of `#EveryoneCanContribute` :wink:)

I was so intrigued and happy for the community I found, that I kept opening and opening MRs and as of know,
December 2022, I have almost 80 merged MRs to my name :smile:

In June, togheter with Lee Tickett and Andrew Smith, we started live streaming our Tuesday Pairing sessions
and then, between August and September 2022 we started formalizing these sessions creating the
[Community Coders](https://gitlab.com/community-coders).

Then, in July 2022 I joined the GitLab Heroes and in August 2022 I got nominated MVP for the 15.3 release :tada: :smile:
